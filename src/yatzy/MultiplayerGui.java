package yatzy;

import javax.swing.*;

public class MultiplayerGui extends GUI{
    private JLabel opponentScoreLabel = new JLabel("  Opponents score:    ");
    private JButton onesButton = new JButton("01");
    private JButton twosButton = new JButton("02");

    public MultiplayerGui(Player p) {
        super(p);
        this.menu.add(this.opponentScoreLabel);
        this.lows.add(this.onesButton);
        this.lows.add(this.onesButton);
        this.lows.add(this.onesButton);
        this.lows.add(this.onesButton);
        this.lows.add(this.onesButton);
        this.lows.add(this.onesButton);
    }

    public void updateOpponentScore(int score) {
        this.opponentScoreLabel.setText("Opponents score: " + score);
    }
}
