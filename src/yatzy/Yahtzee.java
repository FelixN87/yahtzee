package yatzy;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.swing.*;

public class Yahtzee {
    public static void main(String[] args) throws InterruptedException {
//        Game g = new Game();
//        g.singlePlayerGame();
//        g.versusAiGame();
        MainGui mainGui = new MainGui();
        while (!mainGui.isExit) {
            while (!mainGui.isButtonPressed) {
                Thread.sleep(300);
            }
            Game g = new Game();
            if (mainGui.isAiGame) {
                g.versusAiGame();
            } else if (mainGui.isSoloGame) {
                g.singlePlayerGame();
            }
        }
    }
}
