package yatzy;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyToggleButton extends JToggleButton implements ActionListener {
    public boolean isChecked = false;
    public int id;

    public MyToggleButton(int id) {
        super();
        this.id = id;
        this.addActionListener(this);
    }

    public void setChecked(boolean c) {
        this.isChecked = c;
    }

    public boolean getChecked() {
        return this.isChecked;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.isChecked = !this.isChecked;
    }
}
