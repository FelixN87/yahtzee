package yatzy;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainGui implements ActionListener {
    private JButton soloGameButton = new JButton("Solo Game");
    private JButton aiGameButton = new JButton("Against AI");
    private JButton exitButton = new JButton("Exit");
    private JPanel mainPanel = new JPanel();
    private JFrame f = new JFrame();
    public volatile boolean isButtonPressed = false;
    public volatile boolean isSoloGame = false;
    public volatile boolean isAiGame = false;
    public volatile  boolean isExit = false;

    public MainGui() {
        this.soloGameButton.setBounds(150, 100, 200, 100);
        this.aiGameButton.setBounds(150, 250, 200, 100);
        this.exitButton.setBounds(150, 500, 200, 100);
        this.soloGameButton.addActionListener(this);
        this.aiGameButton.addActionListener(this);
        this.exitButton.addActionListener(this);
        this.mainPanel.add(this.soloGameButton);
        this.mainPanel.add(this.aiGameButton);
        this.mainPanel.add(this.exitButton);
        this.f.add(this.mainPanel);
        this.f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.f.setSize(500, 600);
        this.f.setVisible(true);
        this.f.setResizable(false);
        ImageIcon icon = new ImageIcon("C:\\Users\\Felix\\IdeaProjects\\Yahtzee\\src\\yatzy\\icon.png");
        this.f.setIconImage(icon.getImage());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.isButtonPressed = true;
        if (e.getSource() == this.soloGameButton) {
            this.isSoloGame = true;
        }
        if (e.getSource() == this.aiGameButton) {
            this.isAiGame = true;
        }
        if (e.getSource() == this.exitButton) {
            this.isExit = true;
        }
    }
}
