package yatzy;

import java.util.Objects;
import java.util.Random;

public class Dice implements Comparable<Dice>{
    public Integer number;
    public Integer id = -1;

    public Dice() {
        this.throwDice();
    }

    public Dice(Integer n, Integer id) {
        this.number = n;
        this.id = id;
    }

    public void throwDice() {
        Random random = new Random();
        this.number = random.nextInt(6) + 1;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Dice)) {
            return false;
        }
        Dice dice = (Dice) o;
        return Objects.equals(this.id, dice.id) && Objects.equals(this.number, dice.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, id);
    }

    @Override
    public int compareTo(Dice o) {
        if (this.number > o.number) {
            return 1;
        } else if (this.number < o.number) {
            return -1;
        } else {
            return 0;
        }
    }

    public String toString() {
        return String.format("%s", this.number);
    }
}
