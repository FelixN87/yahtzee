package yatzy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class StupidAiPlayer{
    private Hand hand;
    private int totalScore = 0;
    private String[] scores = {"  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  "};
    private List<Integer> choices = new ArrayList(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12));
    private String[][] fakeDices = new String[13][6];

    public StupidAiPlayer() {
        Collections.shuffle(this.choices);
        fakeDices[0] = new String[]{"1,2,5,3,4", "1,1,6,6,2", "2,1,4,1,1", "2,1,1,1,1", "1,1,1,1,1", "3,3,2,6,4"};
        fakeDices[1] = new String[]{"1,2,6,3,3", "2,3,2,1,1", "2,5,6,2,2", "2,2,3,2,2", "2,2,2,2,2", "6,1,6,3,5"};
        fakeDices[2] = new String[]{"4,5,3,2,2", "1,5,3,3,2", "2,3,3,5,3", "4,3,3,3,3", "3,3,3,3,3", "1,1,4,2,6"};
        fakeDices[3] = new String[]{"3,5,5,2,4", "4,4,2,5,1", "2,4,5,4,4", "4,4,1,4,4", "4,4,4,4,4", "3,3,6,1,2"};
        fakeDices[4] = new String[]{"2,3,2,6,5", "2,3,1,5,5", "5,4,5,5,6", "3,5,5,5,5", "5,5,5,5,5", "1,1,1,3,6"};
        fakeDices[5] = new String[]{"1,1,5,3,6", "4,2,6,6,2", "6,2,2,6,6", "6,6,2,6,6", "6,6,6,6,6", "5,2,2,5,4"};
        fakeDices[6] = new String[]{"3,4,3,3,6", "2,1,4,1,1", "5,5,5,2,3", "6,4,1,6,6", "2,2,2,4,5", "4,4,2,2,3"};
        fakeDices[7] = new String[]{"3,3,3,3,6", "1,1,4,1,1", "5,5,5,2,5", "6,6,1,6,6", "2,2,2,2,5", "4,4,2,2,3"};
        fakeDices[8] = new String[]{"2,2,5,5,5", "4,2,2,4,4", "5,1,1,1,5", "6,2,6,6,2", "3,3,3,1,1", "3,2,3,3,1"};
        fakeDices[9] = new String[]{"1,2,3,4,1", "2,3,4,5,2", "6,3,5,4,1", "2,4,5,3,4", "4,5,6,1,3", "1,3,5,5,2"};
        fakeDices[10] = new String[]{"1,2,3,4,5", "2,3,4,5,6", "3,2,5,1,4", "6,5,4,3,2", "2,4,5,3,6", "2,5,3,4,3"};
        fakeDices[11] = new String[]{"2,2,2,2,2", "3,3,4,3,3", "1,2,1,1,1", "5,5,5,5,3", "6,6,6,6,6", "2,3,2,1,4"};
        fakeDices[12] = new String[]{"3,5,4,4,2", "1,6,2,2,4", "3,1,5,3,4", "2,6,6,3,1", "4,3,3,5,4", "2,2,3,1,5"};
    }

    public String playOneRound() {
        String[] methods = {"Ones", "Twos", "Threes", "Fours", "Fives", "Sixes", "Three of a kind", "Four of a kind", "Full house", "Small straight", "Big straight", "Yahtzee", "Chance"};
        int randomMethod = this.choices.get(0);
        int r = randomWithRange(0, 5);
        this.choices.remove(0);
        String dices = this.fakeDices[randomMethod][r];
        System.out.println(methods[randomMethod] + ": " + dices);
        this.makeHand(dices);
        this.scores[randomMethod] = this.hand.findScores().get(randomMethod).toString();
        this.totalScore += this.hand.findScores().get(randomMethod);
        return dices;
    }

    public String[] getScores() {
        return this.scores;
    }

    public int getTotalScore() {
        return this.totalScore;
    }

    public void makeHand(String s) {
        this.hand = new Hand(s);
    }

    private int randomWithRange(int min, int max){
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }
}
