package yatzy;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class GUI implements ActionListener {

    public Player owner;
    public List<MyToggleButton> diceButtons = new ArrayList<>();
    public List<ScoreButton> scoreButtons = new ArrayList<>();
    public List<JLabel> scoreLabels = new ArrayList<>();
    public List<JButton> opponentButtons = new ArrayList<>();
    public String[] scoreLabelText = {"1s", "2s", "3s", "4s", "5s", "6s", "3X", "4X", "FH", "SS", "BS", "YZ", "CH"};
    private final JFrame f = new JFrame();
    protected JPanel lows = new JPanel();
    protected JPanel highs = new JPanel();
    protected JPanel dices = new JPanel();
    protected JPanel menu = new JPanel();
    protected JPanel centerPanel = new JPanel();
    protected JPanel mainPanel = new JPanel();

    private JButton roll = new JButton("Roll 2");
    private JButton play = new JButton("Play");

    private ButtonGroup scoreGroup = new ButtonGroup();

    private JLabel playerScoreLabel = new JLabel("Your score:    ");
    private JLabel opponentScoreLabel = new JLabel("Opponents score:    ");
    private JLabel currentTurn = new JLabel("<html>Current turn: <br/>Player</html>"); //Some magic HTML effery

    private ScoreButton acesButton = new ScoreButton(1, this);
    private ScoreButton twosButton = new ScoreButton(2, this);
    private ScoreButton threesButton = new ScoreButton(3, this);
    private ScoreButton foursButton = new ScoreButton(4, this);
    private ScoreButton fivesButton = new ScoreButton(5, this);
    private ScoreButton sixesButton = new ScoreButton(6, this);
    private JButton playerBonusButton = new JButton();
    private JButton opponentBonusButton = new JButton();

    private ScoreButton threeOfKindButton = new ScoreButton(7, this);
    private ScoreButton fourOfKindButton = new ScoreButton(8, this);
    private ScoreButton fullHouseButton = new ScoreButton(9, this);
    private ScoreButton smallStraightButton = new ScoreButton(10, this);
    private ScoreButton bigStraightButton = new ScoreButton(11, this);
    private ScoreButton yahtzeeButton = new ScoreButton(12, this);
    private ScoreButton chanceButton = new ScoreButton(13, this);

    private JLabel acesLabel = new JLabel("1");
    private JLabel twosLabel = new JLabel("2");
    private JLabel threesLabel = new JLabel("3");
    private JLabel foursLabel = new JLabel("4");
    private JLabel fivesLabel = new JLabel("5");
    private JLabel sixesLabel = new JLabel("6");
    private JLabel bonusLabel = new JLabel("Bonus");

    private JLabel threeOfKindLabel = new JLabel("3X");
    private JLabel fourOfKindLabel = new JLabel("4X");
    private JLabel fullHouseLabel = new JLabel("FH");
    private JLabel smallStraightLabel = new JLabel("SS");
    private JLabel bigStraightLabel = new JLabel("BS");
    private JLabel yahtzeeLabel = new JLabel("Ya");
    private JLabel chanceLabel = new JLabel("Ch");

    private MyToggleButton dice_1 = new MyToggleButton(1);
    private MyToggleButton dice_2 = new MyToggleButton(2);
    private MyToggleButton dice_3 = new MyToggleButton(3);
    private MyToggleButton dice_4 = new MyToggleButton(4);
    private MyToggleButton dice_5 = new MyToggleButton(5);

    public GUI(Player p) {
        this.owner = p;
        this.addButtonsToList();
        f.setTitle("Yahtzee!");
        this.lows.setBackground(Color.yellow);
        this.highs.setBackground(Color.yellow);
        this.dices.setBackground(Color.RED);
        this.menu.setBackground(Color.BLUE);
        this.centerPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.ipady = 40;      //make this component tall
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 1;
        this.centerPanel.add(this.currentTurn, c);
        this.currentTurn.setOpaque(true);
        this.currentTurn.setBackground(Color.cyan);

        this.dices.setPreferredSize(new Dimension(100, 100));
        this.menu.setPreferredSize(new Dimension(100, 50));

        this.menu.setLayout(new FlowLayout());
        this.menu.add(this.playerScoreLabel);
        this.menu.add(this.opponentScoreLabel);

        this.dices.setLayout(new GridLayout(2, 1));
        JPanel diceButtons = new JPanel();
        diceButtons.setLayout(new FlowLayout());
        diceButtons.add(dice_1);
        diceButtons.add(dice_2);
        diceButtons.add(dice_3);
        diceButtons.add(dice_4);
        diceButtons.add(dice_5);
        this.dices.add(diceButtons);

        JPanel playRoll = new JPanel();
        playRoll.setLayout(new FlowLayout());
        playRoll.add(this.roll);
        playRoll.add(this.play);
        this.roll.addActionListener(this);
        this.play.addActionListener(this);
        this.dices.add(playRoll);

        this.diceButtons.add(dice_1);
        this.diceButtons.add(dice_2);
        this.diceButtons.add(dice_3);
        this.diceButtons.add(dice_4);
        this.diceButtons.add(dice_5);

        this.initializeOpponentButtons();

        this.highs.setLayout(new GridLayout(7, 3));
        this.highs.setPreferredSize(new Dimension(150, 50));
        this.highs.add(threeOfKindLabel);
        this.highs.add(threeOfKindButton);
        this.highs.add(this.opponentButtons.get(6));
        this.highs.add(fourOfKindLabel);
        this.highs.add(fourOfKindButton);
        this.highs.add(this.opponentButtons.get(7));
        this.highs.add(fullHouseLabel);
        this.highs.add(fullHouseButton);
        this.highs.add(this.opponentButtons.get(8));
        this.highs.add(smallStraightLabel);
        this.highs.add(smallStraightButton);
        this.highs.add(this.opponentButtons.get(9));
        this.highs.add(bigStraightLabel);
        this.highs.add(bigStraightButton);
        this.highs.add(this.opponentButtons.get(10));
        this.highs.add(yahtzeeLabel);
        this.highs.add(yahtzeeButton);
        this.highs.add(this.opponentButtons.get(11));
        this.highs.add(chanceLabel);
        this.highs.add(chanceButton);
        this.highs.add(this.opponentButtons.get(12));

        this.lows.setLayout(new GridLayout(7,3));
        this.lows.setPreferredSize(new Dimension(150, 50));
        this.lows.add(acesLabel);
        this.lows.add(acesButton);
        this.lows.add(this.opponentButtons.get(0));
        this.lows.add(twosLabel);
        this.lows.add(twosButton);
        this.lows.add(this.opponentButtons.get(1));
        this.lows.add(threesLabel);
        this.lows.add(threesButton);
        this.lows.add(this.opponentButtons.get(2));
        this.lows.add(foursLabel);
        this.lows.add(foursButton);
        this.lows.add(this.opponentButtons.get(3));
        this.lows.add(fivesLabel);
        this.lows.add(fivesButton);
        this.lows.add(this.opponentButtons.get(4));
        this.lows.add(sixesLabel);
        this.lows.add(sixesButton);
        this.lows.add(this.opponentButtons.get(5));
        this.lows.add(this.bonusLabel);
        this.lows.add(this.playerBonusButton);
        this.lows.add(this.opponentBonusButton);
        this.playerBonusButton.setEnabled(false);
        this.opponentBonusButton.setEnabled(false);

        this.mainPanel.setLayout(new BorderLayout());

        this.mainPanel.add(this.lows, BorderLayout.WEST);
        this.mainPanel.add(this.highs, BorderLayout.EAST);
        this.mainPanel.add(this.dices, BorderLayout.SOUTH);
        this.mainPanel.add(this.menu, BorderLayout.NORTH);
        this.mainPanel.add(this.centerPanel, BorderLayout.CENTER);

        f.add(this.mainPanel);

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(500, 600);
        f.setVisible(true);
        f.setResizable(false);
        ImageIcon icon = new ImageIcon("C:\\Users\\Felix\\IdeaProjects\\Yahtzee\\src\\yatzy\\icon.png");
        f.setIconImage(icon.getImage());
//        try {
//            this.printDiceValues();
//        } catch (InterruptedException e) {}
    }

    public void updateDice(int id, int num) {
        this.diceButtons.get(id - 1).setText(String.valueOf(num));
    }

    private void initializeOpponentButtons() {
        for (int i = 0; i < 13; i++) {
            this.opponentButtons.add(new JButton("  "));
            this.opponentButtons.get(i).setEnabled(false);
        }
    }

    public void setOpponentButtonsText(String[] values) {
        for (int i = 0; i < values.length; i++) {
            this.opponentButtons.get(i).setText(values[i]);
        }
    }

    public void printDiceValues() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            System.out.println(this.dice_1.getChecked());
            System.out.println(this.dice_2.getChecked());
            System.out.println(this.dice_3.getChecked());
            System.out.println(this.dice_4.getChecked());
            System.out.println(this.dice_5.getChecked());
            Thread.sleep(10000);
        }
    }

    public void addButtonsToList() {
        this.scoreButtons.add(this.acesButton);
        this.scoreButtons.add(this.twosButton);
        this.scoreButtons.add(this.threesButton);
        this.scoreButtons.add(this.foursButton);
        this.scoreButtons.add(this.fivesButton);
        this.scoreButtons.add(this.sixesButton);
        this.scoreButtons.add(this.threeOfKindButton);
        this.scoreButtons.add(this.fourOfKindButton);
        this.scoreButtons.add(this.fullHouseButton);
        this.scoreButtons.add(this.smallStraightButton);
        this.scoreButtons.add(this.bigStraightButton);
        this.scoreButtons.add(this.yahtzeeButton);
        this.scoreButtons.add(this.chanceButton);

        this.scoreGroup.add(this.acesButton);
        this.scoreGroup.add(this.twosButton);
        this.scoreGroup.add(this.threesButton);
        this.scoreGroup.add(this.foursButton);
        this.scoreGroup.add(this.fivesButton);
        this.scoreGroup.add(this.sixesButton);
        this.scoreGroup.add(this.threeOfKindButton);
        this.scoreGroup.add(this.fourOfKindButton);
        this.scoreGroup.add(this.fullHouseButton);
        this.scoreGroup.add(this.smallStraightButton);
        this.scoreGroup.add(this.bigStraightButton);
        this.scoreGroup.add(this.yahtzeeButton);
        this.scoreGroup.add(this.chanceButton);
    }

    public void setScoreValues(List values) {
        for (int i = 0; i < values.size(); i++) {
            if (!this.scoreButtons.get(i).isDisabled) {
                this.scoreButtons.get(i).setText(String.valueOf(values.get(i)));
            }
        }
    }

    public void clearScoreValues() {
        for (int i = 0; i < 13; i++) {
            if (!this.scoreButtons.get(i).isDisabled) {
                this.scoreButtons.get(i).setText("  ");
            }
        }
    }

    public void playButtonEnabled(boolean enabled) {
        if (enabled)
            this.play.setEnabled(enabled);
    }

    public void disableRollButton() {
        this.roll.setEnabled(false);
    }

    public void enableRollButton() {
        this.roll.setEnabled(true);
    }

    public void disablePlayButton() {
        this.play.setEnabled(false);
    }

    public void enablePlayButton() {
        this.play.setEnabled(true);
    }

    public void disableScoreButtons() {
        for (ScoreButton b : this.scoreButtons) {
            b.setEnabled(false);
        }
    }

    public void enableScoreButtons() {
        for (ScoreButton b : this.scoreButtons) {
            if (!b.isDisabled) {
                b.setEnabled(true);
            }
        }
    }


    public void disableDiceButtons() {
        for (MyToggleButton b : this.diceButtons) {
            b.setEnabled(false);
        }
    }

    public void enableDiceButtons() {
        for (MyToggleButton b : this.diceButtons) {
            b.setEnabled(true);
        }
    }

    public void unselectDiceButtons() {
        for (MyToggleButton b : this.diceButtons) {
            b.setSelected(false);
            b.setChecked(false);
        }
    }

    public List<Integer> getUnSelectedDices() {
        List<Integer> selected = new ArrayList<>();
        for (MyToggleButton b : this.diceButtons) {
            if (!b.getChecked()) {
                selected.add(b.id);
            }
        }
        return selected;
    }

    public ScoreButton getSelectedScoreButton() {
        ScoreButton rButton = null;
        for (ScoreButton b : this.scoreButtons) {
            if (b.isSelected() && !b.isDisabled) {
                System.out.println(b.isSelected() + "   " + b.isDisabled);
                rButton = b;
                break;
            }
        }
        return rButton;
    }

    public int setScoreAndDisable() {
        ScoreButton b = getSelectedScoreButton();
        b.setEnabled(false);
        b.isDisabled = true;
        b.setBackground(Color.GREEN);
        //b.isChecked = false;
        return 0;
    }

    public void printScoreButtons() {
        System.out.println("Disabled Checked");
        for (ScoreButton b : this.scoreButtons) {
            System.out.println(b.isDisabled + " : " + b.isChecked);
        }
        System.out.println("#######################");
    }

    public void updatePlayerScore(int score) {
        this.playerScoreLabel.setText("Your score: " + score);
    }

    public void updateOpponentScore(int score) {
        this.opponentScoreLabel.setText("Opponents score: " + score);
    }

    public void updatePlayerBonusScore(String score) {
        this.playerBonusButton.setText(score);
    }

    public void updateOpponentBonusScore(int score) {
        this.opponentBonusButton.setText(String.valueOf(score));
    }

    public void emptyDiceButtons() {
        for (MyToggleButton b : this.diceButtons) {
            b.setText(" ");
        }
    }

    public int getBonusScore() {
        int bonus = 0;
        for (int i = 0; i < 6; i++) {
            if (this.scoreButtons.get(i).isChecked) {
                bonus += Integer.parseInt(this.scoreButtons.get(i).getText());
            }
        }
        return bonus;
    }

    public void untoggleScoreButtons() {
        this.disablePlayButton();
        for (ScoreButton b : this.scoreButtons) {
            b.setChecked(false);
            b.isChecked = false;
        }
    }

    public void setCurrentTurn(String text) {
        this.currentTurn.setText(text);
    }

    public void updateRollButton(int roll) {
        this.roll.setText("Roll " + roll);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.play) {
            this.owner.setIsPlayed();
        } else {
            this.owner.setIsRolled();
        }
    }
}
