package yatzy;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ScoreButton extends JToggleButton implements ActionListener {
    public boolean isChecked = false;
    public String defaultValue = "   ";
    public boolean isDisabled = false;
    public int id;
    private GUI gui;

    public ScoreButton(int id, GUI gui) {
        super();
        this.gui = gui;
        this.id = id;
        this.addActionListener(this);
        this.setText(this.defaultValue);
    }

    public void displayTemporaryValue() {

    }

    public void setChecked(boolean c) {
        this.isChecked = c;
    }

    public boolean getChecked() {
        return this.isChecked;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.isChecked = !this.isChecked;
        this.gui.playButtonEnabled(this.isChecked);
//        System.out.println("Button pressed is " + this.id);
    }
}
