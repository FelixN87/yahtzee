package yatzy;

import java.util.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Hand {
    public HashMap<String, Integer> hands = new HashMap<>();
    private int lastId = 0;
    private List<Dice> hand = new ArrayList<>();

    public Hand(String d) {
        String[] values = d.split(",");
        int id = 1;
        for (String v : values) {
            this.hand.add(new Dice(Integer.parseInt(v), id));
            id++;
        }
    }

    public Hand(List<Dice> dices) {
        for (Dice dice : dices) {
            this.hand.add(dice);
        }
    }

    public Hand() {
//        List<String> uppers = Arrays.asList("Aces", "Twos", "Threes", "Fours", "Fives", "Sixes");
//        for (String upper : uppers) {
//            hands.put(upper, 0);
//        }
        this.createDices();
    }
    public void createDices() {
        if (!this.hand.isEmpty()) {
            this.hand.clear();
        }
        for (int i = 0; i < 5; i++) {
            this.hand.add(new Dice(this.throwDice(), i + 1)); //Start id with 1 just in case
        }
    }

    public void updateDice(int id) {
        for (Dice d : this.hand) {
            if (d.id == id) {
                d.throwDice();
            }
        }
    }

    public int throwDice() {
        Random random = new Random();
        return random.nextInt( 6) + 1;
    }

    public List<Integer> findScores() {
        List<Integer> scores = new ArrayList<>();
        scores.add(Collections.frequency(this.dicesAsString(), 1) * 1);
        scores.add(Collections.frequency(this.dicesAsString(), 2) * 2);
        scores.add(Collections.frequency(this.dicesAsString(), 3) * 3);
        scores.add(Collections.frequency(this.dicesAsString(), 4) * 4);
        scores.add(Collections.frequency(this.dicesAsString(), 5) * 5);
        scores.add(Collections.frequency(this.dicesAsString(), 6) * 6);

        scores.add(findThreeKind());
        scores.add(findFourKind());
        scores.add(findFullHouse());
        scores.add(findSmallStraight());
        scores.add(findBigStraight());
        scores.add(findYatzy());
        scores.add(findChance());
        return scores;
    }

    public void findUppers() {
        this.hands.put("Aces", Collections.frequency(this.dicesAsString(), 1) * 1);
        this.hands.put("Twos", Collections.frequency(this.dicesAsString(), 2) * 2);
        this.hands.put("Threes", Collections.frequency(this.dicesAsString(), 3) * 3);
        this.hands.put("Fours", Collections.frequency(this.dicesAsString(), 4) * 4);
        this.hands.put("Fives", Collections.frequency(this.dicesAsString(), 5) * 5);
        this.hands.put("Sixes", Collections.frequency(this.dicesAsString(), 6) * 6);
    }

    public void findLowers() {
        this.hands.put("Three of a kind", findThreeKind());
        this.hands.put("Four of a kind", findFourKind());
        this.hands.put("Full House", findFullHouse());
        this.hands.put("Yatzy", findYatzy());
        this.hands.put("Chance", findChance());
        this.hands.put("Small Straight", findSmallStraight());
        this.hands.put("Big Straight", findBigStraight());
    }

    public void addDice(Dice dice) {
        dice.id = this.lastId + 1;
        this.hand.add(dice);
    }

    public void removeDice(Dice dice) {
        hand.remove(dice);
    }

    public List<Dice> getHand() {
        return this.hand;
    }

    public List<Integer> dicesAsString() {
        List<Integer> sDices = new ArrayList<>();
        for (Dice dice : this.hand) {
            sDices.add(dice.number);
        }
        return sDices;
    }
    public String asString() {
        String string = "";
        for (Dice dice : this.hand) {
            string += String.valueOf(dice.number) + " ";
        }
        return string;
    }

    public List<Character> dicesAsCharacter() {
        List<Character> diceChar = new ArrayList<>();
        List<Dice> sortedDice = this.hand.stream().sorted().collect(Collectors.toList());
        for (Dice dice : sortedDice) {
            diceChar.add(Integer.toString(dice.number).charAt(0));
        }
        return diceChar;
    }

    public Integer findThreeKind() {
        if (matcher("nnn??") || matcher("?nnn?") || matcher("??nnn")) {
//            return Integer.parseInt(String.valueOf(dicesAsCharacter().get(2))) * 3;
            return this.dicesAsString().stream().mapToInt(Integer::intValue).sum();
        } else {
            return 0;
        }
    }

    public Integer findFourKind() {
        if (matcher("nnnn?") || matcher("?nnnn")) {
//            return Integer.parseInt(String.valueOf(dicesAsCharacter().get(1))) * 4;
            return this.dicesAsString().stream().mapToInt(Integer::intValue).sum();
        } else {
            return 0;
        }
    }

    public Integer findYatzy() {
        if (matcher("nnnnn")) {
            return 50;
        } else {
            return 0;
        }
    }

    public Integer findFullHouse() {
        if (matcher("nnn??") && matcher("???nn")
                || matcher("nn???") && matcher("??nnn")) {
//            return this.dicesAsString().stream().mapToInt(Integer::intValue).sum();
            return 25;
        } else {
            return 0;
        }
    }

    public Integer findSmallStraight() {
//        if (matcher("12345")) {
//            return 30;
//        } else {
//            return 0;
//        }
        if (matcher("1234?") || matcher("?2345")
                || matcher("2345?") || matcher("?3456")) {
            return 30;
        } else {
            return 0;
        }
    }

    public Integer findChance() {
        return this.dicesAsString().stream().mapToInt(Integer::intValue).sum();
    }

    public Integer findBigStraight() {
        if (matcher("23456") || matcher("12345")) {
            return 40;
        } else {
            return 0;
        }
//        if (matcher("12345", hand) || matcher("23456", hand)) {
//            return true;
//        } else {
//            return false;
//        }
    }

    public boolean matcher(String pattern) {
        //String pattern = "12?4?";
        //List<Character> hand = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand = dicesAsCharacter();
        char n = '0';
        boolean patternMatched = true;
            for (int i = 0; i < pattern.length(); i++) {
                if (!patternMatched) {
                    break;
                }
                if (pattern.charAt(i) == 'n') {
                    //System.out.println("Pattern of n is " + n);
                    if (n == '0') {
                        n = hand.get(i);
                    } else if (hand.get(i) == n) {
                        patternMatched = true;
                    } else {
                        patternMatched = false;
                    }

                } else if (pattern.charAt(i) == '?') {
                    //System.out.println("question mark");
                } else {
                    //System.out.println("Normal pattern");
                    //System.out.println(hand.get(i) + " " + pattern.charAt(i));
                    if ((hand.get(i).equals(pattern.charAt(i)))) {
                        patternMatched = true;
                    } else {
                        patternMatched = false;
                    }
                }
            }
        return patternMatched;
    }
}
