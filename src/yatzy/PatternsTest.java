package yatzy;//package collections.simulator;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//import static collections.simulator.Card.CardSuit.*;
//import static collections.simulator.Card.CardValue.*;
//import static collections.simulator.Helpers.getFlushHand;
//import static collections.simulator.Helpers.getHand;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class PatternsTest {
    @Test
    public void findTriple() {
        List<Character> hand1 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand2 = new ArrayList<Character>(Arrays.asList('5', '5', '5', '6', '5'));
        List<Character> hand3 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '3', '3'));
        List<Character> hand4 = new ArrayList<Character>(Arrays.asList('1', '2', '2', '2', '5'));
        List<String> pattern = new ArrayList<>();
//        assertThat(Patterns.matcher("nnn??", hand2), is(true));
//        assertThat(Patterns.matcher("??nnn", hand3), is(true));
//        assertThat(Patterns.matcher("?nnn?", hand4), is(true));
    }

    @Test
    public void findFourOfAKind() {
        List<Character> hand1 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand2 = new ArrayList<Character>(Arrays.asList('5', '5', '5', '5', '5'));
        List<Character> hand3 = new ArrayList<Character>(Arrays.asList('1', '3', '3', '3', '3'));

        assertThat(Patterns.matcher("nnnn?", hand2), is(true));
        assertThat(Patterns.matcher("nnnn?", hand1), is(false));
        assertThat(Patterns.matcher("?nnnn", hand3), is(true));
    }

    @Test
    public void findFullHouse() {
        List<Character> hand1 = new ArrayList<Character>(Arrays.asList('1', '1', '1', '2', '2'));
        List<Character> hand2 = new ArrayList<Character>(Arrays.asList('3', '3', '5', '5', '5'));
        List<Character> hand3 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '3', '3'));
        List<Character> hand4 = new ArrayList<Character>(Arrays.asList('1', '1', '1', '4', '5'));

        assertThat(Patterns.findFullHouse(hand1), is(true));
        assertThat(Patterns.findFullHouse(hand2), is(true));
        assertThat(Patterns.findFullHouse(hand3), is(false));
        assertThat(Patterns.findFullHouse(hand4), is(false));
    }

    @Test
    public void findYatzy() {
        List<Character> hand1 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand2 = new ArrayList<Character>(Arrays.asList('5', '5', '5', '5', '5'));
        List<Character> hand3 = new ArrayList<Character>(Arrays.asList('3', '3', '3', '3', '3'));
        List<Character> hand4 = new ArrayList<Character>(Arrays.asList('1', '2', '1', '1', '1'));

        assertThat(Patterns.matcher("nnnnn", hand1), is(false));
        assertThat(Patterns.matcher("nnnnn", hand2), is(true));
        assertThat(Patterns.matcher("nnnnn", hand3), is(true));
        assertThat(Patterns.matcher("nnnnn", hand4), is(false));
    }

    @Test
    public void findSmallStraight() {
        List<Character> hand1 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand2 = new ArrayList<Character>(Arrays.asList('1', '3', '4', '5', '6'));
        List<Character> hand3 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '3', '3'));

        assertThat(Patterns.findSmallStraight(hand1), is(true));
        assertThat(Patterns.findSmallStraight(hand2), is(true));
        assertThat(Patterns.findSmallStraight(hand3), is(false));
    }

    @Test
    public void findBigStraight() {
        List<Character> hand1 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand2 = new ArrayList<Character>(Arrays.asList('2', '3', '4', '5', '6'));
        List<Character> hand3 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '3'));

        assertThat(Patterns.matcher("12345", hand1), is(true));
        assertThat(Patterns.matcher("23456", hand2), is(true));
        assertThat(Patterns.matcher("12345", hand3), is(false));
    }
//    @Test
//    public void cardsCanBeCompared() {
//        assertThat(new Card(A, C).compareTo(new Card(A, S)), is(0));
//        assertThat(new Card(A, C).compareTo(new Card(K, S)), is(1));
//        assertThat(new Card(K, H).compareTo(new Card(A, H)), is(-1));
//    }
//
//    @Test
//    public void cardsCanBeSorted() {
//        List<Card> cards = Arrays.asList(
//                new Card(K, H),
//                new Card(Q, H),
//                new Card(J, H),
//                new Card(S10, S),
//                new Card(A, C));
//
//        Collections.sort(cards);
//
//        assertThat(cards.toString(),
//                is("[(S10, S), (J, H), (Q, H), (K, H), (A, C)]"));
//    }
//
//    @Test
//    public void getHandIsHelperMethodForCreatingHandWithTheSpecifiedCards() {
//        Hand hand = getHand("AK"); // suits are arbitrary
//
//        assertThat(hand.toString(), is("[(A, D), (K, C)]"));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsOnePair() {
//        assertThat(getHand("AA").getHandType(),
//                is(HandType.ONE_PAIR));
//        System.out.println(getHand("AA").getHandType());
//        assertThat(getHand("AK").getHandType(),
//                is(not(HandType.ONE_PAIR)));
//        System.out.println(getHand("AK").getHandType());
//        assertThat(getHand("AAKK").getHandType(),
//                is(not(HandType.ONE_PAIR)));
//        System.out.println(getHand("AAKK").getHandType());
//        assertThat(getHand("AAA").getHandType(),
//                is(not(HandType.ONE_PAIR)));
//        System.out.println(getHand("AAA").getHandType());
//        //assertThat(getHand("KKAAA").getHandType(),
//        //        is(not(HandType.ONE_PAIR)));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsTwoPairs() {
//        assertThat(getHand("AAKK").getHandType(),
//                is(HandType.TWO_PAIRS));
//
//        assertThat(getHand("AAKKK").getHandType(),
//                is(not(HandType.TWO_PAIRS)));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsTrips() {
//        assertThat(getHand("AAA").getHandType(),
//                is(HandType.TRIPS));
//
//        assertThat(getHand("AAKKK").getHandType(),
//                is(not(HandType.TRIPS)));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsStraight() {
//        assertThat(getHand("A2345").getHandType(),
//                is(HandType.STRAIGHT));
//        // System.out.println(getHand("TJQKA").getHandType());
//        assertThat(getHand("TJQKA").getHandType(),
//                is(HandType.STRAIGHT));
//
//        assertThat(getHand("2345").getHandType(),
//                is(not(HandType.STRAIGHT)));
//
//        assertThat(getHand("23567").getHandType(),
//                is(not(HandType.STRAIGHT)));
//
//        assertThat(getHand("JQKA2").getHandType(),
//                is(not(HandType.STRAIGHT)));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsFlush() {
//        assertThat(getFlushHand("23567").getHandType(),
//                is(HandType.FLUSH));
//
//        assertThat(getHand("23456").getHandType(),
//                is(not(HandType.FLUSH)));
//
//        assertThat(getFlushHand("23456").getHandType(),
//                is(not(HandType.FLUSH)));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsFullHouse() {
//        assertThat(getFlushHand("AKAAK").getHandType(),
//                is(HandType.FULL_HOUSE));
//
//        assertThat(getHand("AAAAK").getHandType(),
//                is(not(HandType.FULL_HOUSE)));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsFourOfAKind() {
//        assertThat(getHand("AAAA").getHandType(),
//                is(HandType.FOUR_OF_A_KIND));
//    }
//
//    @Test
//    public void handKnowsWhetherItContainsStraightFlush() {
//        assertThat(getFlushHand("23456").getHandType(),
//                is(HandType.STRAIGHT_FLUSH));
//
//        assertThat(getHand("23456").getHandType(),
//                is(not(HandType.STRAIGHT_FLUSH)));
//
//        assertThat(getFlushHand("23567").getHandType(),
//                is(not(HandType.STRAIGHT_FLUSH)));
//    }
}
