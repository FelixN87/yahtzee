package yatzy;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class YahtzeeTests {
    @Test
    public void dicesEquality() {
        assertThat(new Dice(2, 1).equals(new Dice(2, 1)), is(true));
        assertThat(new Dice(2, 1).equals(new Dice(2, 2)), is(false));
    }

    @Test
    public void canSortHand() {
//        assertThat(new Card(A, C).compareTo(new Card(A, S)), is(0));
//        assertThat(new Card(A, C).compareTo(new Card(K, S)), is(1));
//        assertThat(new Card(K, H).compareTo(new Card(A, H)), is(-1));
        List<Dice> hand = Arrays.asList(new Dice(4, 1), new Dice(5, 2), new Dice(3, 3), new Dice(6, 4), new Dice(1, 5));
        Collections.sort(hand);
        assertThat(hand.toString(), is("[1, 3, 4, 5, 6]"));
        List<Dice> hand2 = Arrays.asList(new Dice(2, 1), new Dice(3, 2), new Dice(4, 3), new Dice(5, 4), new Dice(6, 5));
        Collections.sort(hand2);
        assertThat(hand2.toString(), is("[2, 3, 4, 5, 6]"));
    }

    @Test
    public void handStringify() {
        Hand hand = new Hand();
        hand.addDice(new Dice(6, 1));
        hand.addDice(new Dice(5, 2));
        hand.addDice(new Dice(4, 3));
        hand.addDice(new Dice(5, 4));
        hand.addDice(new Dice(2, 5));
        //System.out.println(hand.dicesAsString());
//        int i = Collections.frequency(hand.dicesAsString(), 5);
//        System.out.println(i);
    }

    @Test
    public void findsUppers() {
        Hand hand = new Hand();
        hand.addDice(new Dice(6, 1));
        hand.addDice(new Dice(5, 2));
        hand.addDice(new Dice(6, 3));
        hand.addDice(new Dice(5, 4));
        hand.addDice(new Dice(5, 5));
        hand.findUppers();
        assertThat(hand.hands.get("Sixes"), is(12));
        assertThat(hand.hands.get("Fives"), is(15));
        assertThat(hand.hands.get("Aces"), is(0));
        assertThat(hand.hands.get("Twos"), is(0));
        assertThat(hand.hands.get("Threes"), is(0));
        assertThat(hand.hands.get("Fours"), is(0));
    }

    @Test
    public void findFullHouse() {
        Hand hand1 = createHandFromString("11222");
        Hand hand2 = createHandFromString("34334");
        assertThat(hand1.findFullHouse(), is(8));
        assertThat(hand1.findFullHouse(), not(18));
        assertThat(hand2.findFullHouse(), is(17));
    }

    @Test
    public void findSmallStraight() {
        Hand hand1 = createHandFromString("25413");
        Hand hand2 = createHandFromString("11234");
        assertThat(hand1.findSmallStraight(), is(15));
        assertThat(hand2.findSmallStraight(), is(0));
    }

    @Test
    public void findBigStraight() {
        Hand hand1 = createHandFromString("26354");
        Hand hand2 = createHandFromString("26344");
        assertThat(hand1.findBigStraight(), is(20));
        assertThat(hand2.findBigStraight(), is(0));
    }

    @Test
    public void findChance() {
        Hand hand1 = createHandFromString("12345");
        Hand hand2 = createHandFromString("66666");
        assertThat(hand1.findChance(), is(15));
        assertThat(hand2.findChance(), is(30));
    }

    @Test
    public void findYatzy() {
        Hand hand1 = createHandFromString("11111");
        Hand hand2 = createHandFromString("33333");
        Hand hand3 = createHandFromString("11211");
        assertThat(hand1.findYatzy(), is(50));
        assertThat(hand2.findYatzy(), is(50));
        assertThat(hand3.findYatzy(), not(50));
    }

    @Test
    public void findThreeOfAKind() {
        Hand hand1 = createHandFromString("12151");
        Hand hand2 = createHandFromString("22233");
        Hand hand3 = createHandFromString("23512");
        assertThat(hand1.findThreeKind(), is(3));
        assertThat(hand2.findThreeKind(), is(6));
        assertThat(hand3.findThreeKind(), is(0));
    }

    @Test
    public void findFourOfAKind() {
        Hand hand1 = createHandFromString("12111");
        Hand hand2 = createHandFromString("22232");
        Hand hand3 = createHandFromString("23212");
        assertThat(hand1.findFourKind(), is(4));
        assertThat(hand2.findFourKind(), is(8));
        assertThat(hand3.findFourKind(), is(0));
    }

    public Hand createHandFromString(String dices) {
        List<Dice> dice = new ArrayList<>();
        for (int i = 0; i < dices.length(); i++) {
            dice.add(new Dice(Integer.parseInt(String.valueOf(dices.charAt(i))), i + 1));
        }
        return new Hand(dice);
    }
}
