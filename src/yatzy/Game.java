package yatzy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Game {
    private List<Integer> scoreValues = new ArrayList<>();
    private Player player1;
    private Player player2;
    private StupidAiPlayer aiPlayer;
    private Player currentPlayer;
    public Game() {
        this.player1 = new Player();
        this.currentPlayer = this.player1;
//        System.out.println(this.hand.asString());
    }

    public void singlePlayerGame() {
        currentPlayer.disableOpponentScores();
        for (int i = 0; i < 13; i++) {
            currentPlayer.playOneRound();
            currentPlayer.saveScore();
//            this.changeCurrentPlayer();
//            currentPlayer.getNewHand();
        }
    }

    public void versusAiGame() throws InterruptedException {
        this.aiPlayer = new StupidAiPlayer();
        String aiDice = "";
        for (int i = 0; i < 13; i++) {
            this.currentPlayer.displayVictoryScreen("<html>Current turn:<br>Player</html>");
            this.currentPlayer.playOneRound();
            this.currentPlayer.saveScore();
            this.currentPlayer.disablePlayerControls();
            this.currentPlayer.displayVictoryScreen("<html>Current turn:<br>Computer</html>");
            Thread.sleep(1000);
            aiDice = this.aiPlayer.playOneRound();
            Thread.sleep(500);
            this.currentPlayer.setDiceValues(aiDice);
            Thread.sleep(1000);
            this.currentPlayer.setOpponentScore(this.aiPlayer.getScores(), this.aiPlayer.getTotalScore());
            Thread.sleep(800);
            this.currentPlayer.enablePlayerControls();
        }
        this.getWinner();
    }

    private void getWinner() {
        if (this.currentPlayer.totalScore > this.aiPlayer.getTotalScore()) {
            this.currentPlayer.displayVictoryScreen("You won!");
        } else {
            this.currentPlayer.displayVictoryScreen("You lost!");
        }
    }

    private void changeCurrentPlayer() {
        if (this.currentPlayer.equals(this.player1)) {
            this.currentPlayer = this.player2;
        } else {
            this.currentPlayer = this.player1;
        }
    }

    public void playGameLoop() {
        for (int i = 0; i < 13; i++) {

        }
    }
}
