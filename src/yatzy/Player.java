package yatzy;

import javax.swing.*;
import java.util.List;

public class Player {
    private String id;
    private String name;
    private GUI gui;
    private Hand hand;
    private volatile boolean rolled = false;
    private volatile boolean played = false;
    private int numberOfRolls = 3;
    public int totalScore = 0;
    private int bonusScore = 0;
    private boolean bonusGet = false;

    public Player() {
        this.gui = new GUI(this);
        this.hand = new Hand();
        this.initializeDiceGui();
        this.updateScores();
        this.gui.disablePlayButton();
    }

    public List<Integer> getScores() {
        return this.hand.findScores();
    }

    public void initializeDiceGui() {
        this.gui.unselectDiceButtons();
        this.updateDiceButtonValues();
    }

    public void updateDiceButtonValues() {
        for (Dice d : this.hand.getHand()) {
            this.gui.updateDice(d.id, d.number);
        }
    }

    public void setDiceValues(String v) {
        String[] values = v.split(",");
        for (int i = 0; i < values.length; i++) {
            this.gui.updateDice(i + 1, Integer.parseInt(values[i]));
        }
    }

    public void updateDices() {
        List<Integer> selected = this.gui.getUnSelectedDices();
        if (!selected.isEmpty()) {
            for (Integer n : selected) {
                this.hand.updateDice(n);
            }
        }
    }

    public void saveScore() {
        ScoreButton button = this.gui.getSelectedScoreButton();
        this.totalScore += this.getScores().get(button.id - 1);
        this.bonusScore += this.gui.getBonusScore();
        this.gui.updatePlayerScore(this.totalScore);
        this.updateBonusScore();
        button.isChecked = false;
        this.gui.setScoreAndDisable();
        this.gui.clearScoreValues();
    }

    private void updateBonusScore() {
        if (!this.bonusGet) {
            if (this.bonusScore >= 63) {
                this.gui.updatePlayerBonusScore("35");
                this.totalScore += 35;
                this.bonusGet = true;
            } else {
                this.gui.updatePlayerBonusScore(String.valueOf(this.bonusScore));
            }
        }
    }

    public void updateScores() {
        this.gui.setScoreValues(this.getScores());
    }

    public void disableOpponentScores() {
        for(JButton b : this.gui.opponentButtons) {
            b.setEnabled(false);
        }
    }

    public void disablePlayerControls() {
        this.gui.disableRollButton();
        this.gui.disablePlayButton();
        this.gui.disableScoreButtons();
        this.gui.disableDiceButtons();
        this.gui.emptyDiceButtons();
    }

    public void enablePlayerControls() {
        this.gui.enableRollButton();
        this.gui.enablePlayButton();
        this.gui.enableScoreButtons();
        this.gui.enableDiceButtons();
    }

    public void playOneRound() {
        this.enablePlayRollButtons();
        this.gui.disablePlayButton();
        this.getNewHand();
        for (int i = 0; i < 3; i++) {
            if (waitRollOrPlay()) {
                break;
            }
        }
        this.disablePlayRollButtons();
    }

    public boolean waitRollOrPlay() {
        while (!this.played && !this.rolled) {
        }
        if (this.rolled) {
            this.rolled = false;
            this.numberOfRolls--;
            this.gui.updateRollButton(this.numberOfRolls - 1);
            if (this.numberOfRolls == 1) {
                this.gui.disableRollButton();
            }
//            this.hand.createDices();
            this.updateDices();
            this.updateDiceButtonValues();
            this.updateScores();
            this.gui.untoggleScoreButtons();
            return false;
        } else {
//            this.gui.setScoreAndDisable();
//            this.gui.printScoreButtons();
            this.numberOfRolls = 3;
//            this.gui.unselectDiceButtons();
            return true;
        }
    }

    public void setOpponentScore(String[] scores, int total) {
        this.gui.setOpponentButtonsText(scores);
        this.gui.updateOpponentScore(total);
    }

    public void setIsRolled() {
        this.rolled = true;
    }

    public void setIsPlayed() {
        this.played = true;
    }

    public void disablePlayRollButtons() {
        this.gui.disablePlayButton();
        this.gui.disableRollButton();
    }

    public void enablePlayRollButtons() {
        this.rolled = false;
        this.played = false;
        this.gui.enablePlayButton();
        this.gui.enableRollButton();
        this.gui.updateRollButton(2);
    }

    public void getNewHand() {
        this.hand.createDices();
        this.initializeDiceGui();
        this.updateScores();
    }


    public String getName() {
        return this.name;
    }

    public void displayVictoryScreen(String s) {
        this.gui.setCurrentTurn(s);
    }
}
