package yatzy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Patterns {
    public static void main(String[] args) {
        List<Character> hand1 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand2 = new ArrayList<Character>(Arrays.asList('3', '5', '5', '5', '5'));
        List<Character> hand3 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        List<Character> hand4 = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        System.out.println(matcher("12345", hand1));
        System.out.println(matcher("nnnn?", hand2));
        System.out.println(matcher("?2345", hand3));
        System.out.println(matcher("1334?", hand4));
    }

    public static boolean findFullHouse(List<Character> hand) {
        if (matcher("nnn??", hand) && matcher("???nn", hand)
        || matcher("nn???", hand) && matcher("??nnn", hand)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean findSmallStraight(List<Character> hand) {
        if (matcher("1234?", hand) || matcher("?2345", hand)
                || matcher("2345?", hand) || matcher("?3456", hand)) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean matcher(String pattern, List<Character> hand) {
        //String pattern = "12?4?";
        //List<Character> hand = new ArrayList<Character>(Arrays.asList('1', '2', '3', '4', '5'));
        char n = '0';
        boolean patternMatched = true;
        for (int i = 0; i < pattern.length(); i++) {
            if (!patternMatched) {
                return patternMatched;
            }
            if (pattern.charAt(i) == 'n') {
                //System.out.println("Pattern of n is " + n);
                if (n == '0') {
                    n = hand.get(i);
                } else if (hand.get(i) == n) {
                    patternMatched = true;
                } else {
                    patternMatched = false;
                }

            } else if (pattern.charAt(i) == '?') {
                //System.out.println("question mark");
            } else {
                //System.out.println("Normal pattern");
                //System.out.println(hand.get(i) + " " + pattern.charAt(i));
                if ((hand.get(i).equals(pattern.charAt(i)))) {
                    patternMatched = true;
                } else {
                    patternMatched = false;
                }
            }
        }
        return patternMatched;
    }
}
